<?php
namespace App\Models;
use CodeIgniter\Model;

class ContactUsModel extends Model {
    protected $table = 'contact_us';
    protected $primaryKey = 'ID'; 
    protected $useAutoIncrement = true;
    protected $returnType = 'object';
    protected $allowedFields = [
        'name',
        'email',
        'message',
        'id_file'
    ];
    function insert_contact($name, $email, $message=null, $id_file=null){
        $data = [
            'name' => $name,
            'email' => $email,
            'message' => $message,
            'id_file' => $id_file
        ];
        $this->insert($data);
        return $this->insertID();
    }
}
