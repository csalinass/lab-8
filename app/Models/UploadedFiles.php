<?php

namespace App\Models;

use CodeIgniter\Model;

class UploadedFiles extends Model
{
    protected $table = 'uploaded_files';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['client_name', 'client_extention', 'real_name', 'url_file', 'dir_file'];

    public function insertUploadFile($client_name, $client_extension, $real_name, $url_file, $dir_file)
    {
        $data = [
            'client_name' => $client_name,
            'client_extention' => $client_extension,
            'real_name' => $real_name,
            'url_file' => $url_file,
            'dir_file' => $dir_file
        ];

        $this->insert($data);
        return $this->insertID();
    }

    public function removeUploadFile($id_file)
    {
        $this->delete($id_file, true);
    }

    public function getUploadFile($id_file)
    {
        return $this->select('*')
            ->where('id', $id_file)
            ->get()
            ->getRow();
    }
}
