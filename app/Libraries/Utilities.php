<?php
namespace App\Libraries;
use App\Models\UploadedFiles;
class Utilities{
    protected $upFiles;

    public function __construct(){
        $this->upFiles = new UploadedFiles();

    }
    public function keep_uploaded_file($file, $uploaded_dir){
        $newName = $file->getRandomName();

        $id_file = $this->upFiles->insertUploadFile(
            $file->getClientName(),
            $file->guessExtension(),
            $newName,
            '/uploads/'.$uploaded_dir."/".$newName,
            getcwd().'/uploades/'.$uploaded_dir."/".$newName
        );

        $file->move(getcwd()."/uploades/".$uploaded_dir."/".$newName);
        
        return $id_file;
    }

    public function delete_uploaded_file($id_file){
        $file = $this->upFiles->getUploadedFile($id_file);

        if (is_file($file->dir_file)) {
            unlink($file->dir_file);
            }
            
        
            $this->upFiles->removeUploadedFile($id_file);
        
    }
}