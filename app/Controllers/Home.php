<?php

namespace App\Controllers;

use App\Models\ContactUsModel;
use App\Libraries\Utilities;
use App\Models\UploadedFiles;
use App\Libraries\CiOAuth;
use OAuth2\Request;

class Home extends BaseController
{
    protected $contactModel;
    protected $utilities;
    protected $ci_oauth;
    protected $oauth_request;
    protected $oauth_respond;

    public function __construct()
    {
        $this->contactModel = new ContactUsModel();
        $this->utilities = new Utilities();
        $this->ci_oauth = new CiOAuth();
        $this->oauth_request = new Request();
    }

    public function index(): string
    {
        // Cargar la vista perritos_view.php
        return view('perritos_view');
    }

    public function contact_keep()
    {
        $id_file = ($this->request->getFile("file_1") !== null) ? $this->utilities->keep_uploaded_file($this->request->getFile("file_1"), 'contact') : null;
        
        $this->contactModel->insert_contact(
            $this->request->getVar('name'),
            $this->request->getVar('email'),
            $this->request->getVar('message'),
            $id_file // Asegúrate de pasar el ID del archivo aquí
        );

        return $this->respond([
            'code' => 200
        ]);
    }
    
    public function login_creds()
    {
        $this->oauth_respond = $this->ci_oauth->server->handleTokenRequest(
            $this->oauth_request->createFromGlobals()
        );
        
        $code = $this->oauth_respond->getStatusCode();
        $body = $this->oauth_respond->getResponseBody();
        
        if ($code == 200) {
            return $this->respond([
                'code' => $code,
                'data' => json_decode($body),
                'authorized' => $code
            ]);
        } else {
            return $this->fail(json_decode($body));
        }
    }
}